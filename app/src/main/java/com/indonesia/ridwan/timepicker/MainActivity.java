package com.indonesia.ridwan.timepicker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends AppCompatActivity {


    public String currentTime(){

        String mcurrentTime = "Time: "+ timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute();
        return mcurrentTime;
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timePicker = (TimePicker) findViewById(R.id.timepicker);
        displayTime = (TextView) findViewById(R.id.txtv);
        changetime = (Button) findViewById(R.id.bchngetime);

        timePicker.setIs24HourView(true);
        displayTime.setText(currentTime());
        changetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayTime.setText(currentTime());
            }
        });
    }

    TimePicker timePicker;
    TextView displayTime;
    Button changetime;

}
